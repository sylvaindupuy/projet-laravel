<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('types')->insert(
            array(
                'id' => 1,
                'name' => 'breakfast'
            )
        );

        DB::table('types')->insert(
            array(
                'id' => 2,
                'name' => 'Lunch'
            )
        );

        DB::table('types')->insert(
            array(
                'id' => 3,
                'name' => 'Snack'
            )
        );

        DB::table('types')->insert(
            array(
                'id' => 4,
                'name' => 'dinner'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
