@extends('layouts.app')

@section('content')
<div class="container">
    <h2>List of the meals</h2>

    <div class="row">
        <ul>
            @forelse ($dates as $date => $meals)
                <span>{{$date}}, total energy for the day : {{$energyByDate[$date]." kcal"}}</span>
                <ul>
                @foreach ($meals as $meal)
                    <li class="meal">
                        <p>{{ $meal->type->name }}</p>
                        <a href="{{route('meals.meal',[$meal->id])}}" class="btn btn-primary" role="button">More info</a>
                        <a href="{{route('meals.modify',[$meal->id])}}" class="btn btn-warning" role="button">Modify</a>
                        <form action="{{route('meals.formDelete',[$meal->id])}}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>            
                        </form>
                    </li>
                @endforeach
                </ul>

            @empty
                <li>Aucun repas</li>
            @endforelse
        </ul>
    </div>


    <a href="{{route('meals.add')}}" class="btn btn-outline-dark" role="button">Add a meal</a>
</div>
@endsection