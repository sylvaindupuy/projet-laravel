@extends('layouts.app')

@section('content')
<div class="container">

    <h1 style="text-align: center;">Statistics</h1>

    <h2>Worst products (top 5)</h2>
    <div class="row">
        <ul>
            @forelse ($worstProducts as $product)
                <li><img src="{{$product->img_link}}" alt="product image" width="67" height="100"> {{$product->name}} ({{$product->energy_value." kcal"}})</li>
            @empty
                <li>No product...</li>
            @endforelse
        </ul>
    </div>

    <h2>Best products (top 5)</h2>
    <div class="row">
        <ul>
            @forelse ($bestsProducts as $product)
                <li><img src="{{$product->img_link}}" alt="product image" width="67" height="100"> {{$product->name}} ({{$product->energy_value." kcal"}})</li>
            @empty
                <li>No product...</li>
            @endforelse
        </ul>
    </div>

    <h2>Worst products total energy (all meals) (top 5)</h2>
    <div class="row">
        <ul>
            @forelse ($totalEnergyProducts as $product)
                <li>
                    <img src="{{$product->img_link}}" alt="product image" width="67" height="100">
                    {{$product->name}} ({{$product->meals->count()}} * {{$product->energy_value}} -> {{$product->meals->count() * $product->energy_value}} kcal)
                </li>
            @empty
                <li>No product...</li>
            @endforelse
        </ul>
    </div>

</div>
@endsection