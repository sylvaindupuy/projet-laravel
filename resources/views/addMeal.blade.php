@extends('layouts.app')

@section('content')
<div class="container">
    @isset ($meal)
        <h1>Modify a meal</h1>
    @else
        <h1>Add a meal</h1>
    @endisset
    <div class="row justify-content-center">
        @isset ($meal)
            <form action="{{route('meals.formModify')}}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="mealDate">Date of the meal</label>
                    <input type="date" class="form-control" id="mealDate" name="mealDate" value="{{ $meal->date }}" required>
                </div>
                <div class="form-group">
                    <label for="mealType">Type of meal</label>
                    <select class="form-control" id="mealType" name="mealType">
                        @foreach ($types as $type)
                            <option value="{{ $type->id }}" @if($meal->type->id === $type->id) selected @endif>{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="id" value="{{ $meal->id }}">
                <button type="submit" class="btn btn-primary mb-2">Update meal</button>
            </form>
        @else
            <form method="POST">
                @csrf
                <div class="form-group">
                    <label for="mealDate">Date of the meal</label>
                    <input type="date" class="form-control" id="mealDate" name="mealDate" required>
                </div>
                <div class="form-group">
                    <label for="mealType">Type of meal</label>
                    <select class="form-control" id="mealType" name="mealType">
                        @foreach ($types as $type)
                            <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mb-2">Add meal</button>
            </form>
        @endisset
    </div>
    <a href="{{route('meals')}}" class="btn btn-outline-dark" role="button">Back to meals</a>
</div>
@endsection