@extends('layouts.app')

@section('content')
<div class="container">

    @isset ($meal)
    <div class="row justify-content-center">
        <h2>Meal : {{ $meal->date }} - {{ $meal->type->name }}</h2>
    </div>

    <h2>products :</h2>
    <ul>
        @forelse ($meal->products as $product)
            <li><img src="{{$product->img_link}}" alt="product image" width="67" height="100"> {{$product->name}} ({{$product->energy_value." kcal"}})</li>
        @empty
            <li>No product...</li>
        @endforelse
    </ul>

    <h3>Total : {{ $meal->totalEnergy()." kcal" }}</h3>

    <div class="row" style="padding: 15px">
        <form action="{{route('meals.formAddProduct')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="barCode">Bar code</label>
                <input type="number" class="form-control" id="barCode" name="barCode" maxlength="19" required>
            </div>

            <input type="hidden" name="mealId" value="{{ $meal->id }}">

            <button type="submit" class="btn btn-primary mb-2">Add product</button>
        </form>
    </div>
    <a href="{{route('meals.modify',[$meal->id])}}" class="btn btn-warning" role="button">Modify</a>
    @else
        <h2>This meal doesn't exist</h2>
    @endisset

    <a href="{{route('meals')}}" class="btn btn-outline-dark" role="button">Back to meals</a>
</div>
@endsection