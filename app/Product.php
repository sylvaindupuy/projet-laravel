<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Get meals that get the product
     */
    public function meals()
    {
        return $this->belongsToMany('App\Meal');
    }
}
