<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    /**
     * Get the type of meal associated
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * Get the products of meal
     */
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    /**
     * calculate total energetic value 
     */
    public function totalEnergy(){

        $total = 0;
        
        foreach ($this->products as $product) {
            $total += $product->energy_value;
        }

        return $total;
    }

}
