<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Meal;
use App\Product;

class ProductController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /******************************* FORMS ********************************/

    public function formAddProduct(Request $request){
        if($request->has('barCode') && $request->has('mealId')){

            $meal = Meal::where('user_id', Auth::id())->with('type')->find($request->mealId);
            $productDB = Product::where('barcode', $request->barCode)->first();

            if(empty($productDB)){
                $response = $this->client->request("GET", config('openfoodfacts.API_URL').$request->barCode, [
                    "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json"
                ]]);

                if( $response->getStatusCode() === 200){
                    $product = json_decode($response->getBody(), true)['product'];

                    $productA = new Product;
                    $productA->name = $product['product_name'];
                    $productA->img_link = $product['image_thumb_url'];
                    $productA->barcode = $request->barCode;

                    // on calcule le ratio (ex: 2 portion de 90g, on doit multiplier par 2)
                    if(array_key_exists('serving_quantity', $product) && array_key_exists('product_quantity', $product)){
                        $ratio = $product['product_quantity'] / $product['serving_quantity'];
                        $energy = $product['nutriments']['energy_serving'];

                    } else if(array_key_exists('product_quantity', $product) && array_key_exists('energy_100g', $product['nutriments'])) {
                        $ratio = $product['product_quantity'] / 100;
                        $energy = $product['nutriments']['energy_100g'];

                    } else {
                        $ratio = 1;
                        $energy = $product['nutriments']['energy'];
                    }

                    /* / 4.184 : convert Kj -> kcal */
                    $productA->energy_value = round(($energy * $ratio) / 4.184);

                    $meal->products()->save($productA);
                }
            } else {
                $meal->products()->save($productDB);
            }

            return redirect()->route('meals.meal', ['id' => $request->mealId]);
        }
    }
}
