<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Meal;
use App\Product;

class statisticController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('energy_value', 'ASC')->with('meals')->get();

        // 5 first
        $bestsProducts = clone $products;
        $bestsProducts = $bestsProducts->splice(0, 5);

        $products = $products->reverse();

        // 5 last (reverse)
        $worstProducts = clone $products;
        $worstProducts = $worstProducts->splice(0, 5);

        // sort by total
        $totalEnergyProducts = $products->sortBy(function($product){
            return -($product->meals->count() * $product->energy_value);
        });

        return view('statistics', compact('worstProducts', 'bestsProducts', 'totalEnergyProducts'));
    }
}
