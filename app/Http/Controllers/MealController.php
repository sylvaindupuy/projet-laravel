<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Meal;
use App\Type;

class MealController extends Controller
{
    /******************************* VIEWS ********************************/
    public function index()
    {
        $types = Type::get();
        $meals = Meal::where('user_id', Auth::id())->with('type')->get();

        $energyByDate = array();
        $dates = array();

        // group by date
        foreach($meals as $meal)
        { 
            $dates[$meal->date][] = $meal;

            if(array_key_exists($meal->date, $energyByDate)){
                $energyByDate[$meal->date] += $meal->totalEnergy();
            } else {
                $energyByDate[$meal->date] = $meal->totalEnergy();
            }
        }

        return view('meals', compact('dates', 'energyByDate', 'types'));
    }

    public function meal($id){        
        $meal = Meal::where('user_id', Auth::id())->with('type')->find($id);
        return view('meal', compact('meal'));
    }

    public function addMeal(){
        $types = Type::get();
        return view('addMeal', compact('types'));
    }

    public function modifyMeal($id){
        $types = Type::get();
        $meal = Meal::where('user_id', Auth::id())->with('type')->find($id);
        return view('addMeal', compact('meal', 'types'));
    }

    /******************************* FORMS ********************************/

    public function formAddMeal(Request $request){
        if($request->has('mealDate') && $request->has('mealType')){
            $meal = new Meal;

            $meal->date = $request->mealDate;
            $meal->type_id = $request->mealType;
            $meal->user_id = Auth::id();

            $meal->save();

            return redirect('meals');
        }
    }

    public function formDeleteMeal($id){
        Meal::destroy($id);
        return redirect('meals');
    }

    public function formModifyMeal(Request $request){
        if($request->has('id') && $request->has('mealDate') && $request->has('mealType')){
            $meal = Meal::find($request->id);

            $meal->date = $request->mealDate;
            $meal->type_id = $request->mealType;

            $meal->save();
        }

        return redirect('meals');
    }
}
