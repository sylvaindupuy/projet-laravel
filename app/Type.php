<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * Get the meals associated
     */
    public function meal()
    {
        return $this->hasMany('App\Meal');
    }
}
