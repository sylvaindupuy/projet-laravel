<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// ROUTES
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/meals', ['middleware' => 'auth', 'uses' => 'MealController@index'])->name('meals');
Route::get('/meals/add', ['middleware' => 'auth', 'uses' => 'MealController@addMeal'])->name('meals.add');
Route::get('/meals/modify/{id}', ['middleware' => 'auth', 'uses' => 'MealController@modifyMeal'])->name('meals.modify');
Route::get('/meals/{id}', ['middleware' => 'auth', 'uses' => 'MealController@meal'])->name('meals.meal');
Route::get('/statistics', ['middleware' => 'auth', 'uses' => 'StatisticController@index'])->name('statistics');

// FORM
Route::post('/meals/add', ['middleware' => 'auth', 'uses' => 'MealController@formAddMeal'])->name('meals.formAdd');
Route::put('/meals/modify', ['middleware' => 'auth', 'uses' => 'MealController@formModifyMeal'])->name('meals.formModify');
Route::delete('meals/{id}', ['middleware' => 'auth', 'uses' => 'MealController@formDeleteMeal'])->name('meals.formDelete');
Route::post('meals/product/add', ['middleware' => 'auth', 'uses' => 'ProductController@formAddProduct'])->name('meals.formAddProduct');