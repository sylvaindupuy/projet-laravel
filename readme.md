# Projet laravel

Application Laravel pour pouvoir mesurer la consommation en calories (kcal) des repas des utilisateurs.

## Launch

Rename .env.exemple to .env

Setup database in the .env file

```bash
composer update
npm install
npm run dev
php artisan serve
```

## tools

```bash
composer update
php artisan migrate:refresh
php artisan make:[model-migration-controller]
npm run watch
```

1 kJ = 0.238846 kcal


Sylvain Dupuy